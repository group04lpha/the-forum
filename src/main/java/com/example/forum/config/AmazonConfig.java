package com.example.forum.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmazonConfig {
    @Bean
    public AmazonS3 s3() {
        AWSCredentials awsCredentials =
                new BasicAWSCredentials("AKIA2SM25QM3XNEU4AH4", "VpEZ5hbLAG2Xz8FrSP2Yd1ivr2GsijSc/+9SelBJ");
        return AmazonS3ClientBuilder
                .standard()
                .withRegion(region())
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();

    }

    @Bean
    public Regions region() {
        return Regions.EU_CENTRAL_1;
    }
}
